#
# Replays contents of ../ALL.TXT into any wsjtx client listening on a given socket using Decode packets.
# Primary usage is to test GridTracker's CallRoster. Be careful to concatenate ALL.TXT to the relevant
# portion, otherwise the port will be flooded with decode messages. To use it with running GT, place
# ALL.TXT into root dir of project, set below forwarding ports to whatever GT listens on (WSJT-X server
# port) and run this program.

FORWARDING_PORTS = [
    # { 'ip_address': '127.0.0.1', 'port': 2245, 'timeout':0.2 },
    { 'ip_address':'224.0.0.1', 'port':2237, 'timeout':0.2 }
]

import os
import sys
import logging

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import pywsjtx
import pywsjtx.extra.simple_server

def main():
    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s]  %(message)s")
    consoleFormatter = logging.Formatter('%(asctime)s %(message)s')
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')
    wsjtx_instance_addr_port = None
    servers = []
    for port_spec in FORWARDING_PORTS:
        servers.append({"instance":pywsjtx.extra.simple_server.SimpleServer(port_spec.get('server_ip_address','127.0.0.1'),
                                                                           port_spec.get('server_port',0), timeout=port_spec['timeout']),
                        "to_addr_port": (port_spec['ip_address'], port_spec['port']),
                        "server_spec": port_spec })

        allTxt = open('ALL.TXT', 'r')
        lines = allTxt.readlines()
        
        for line in lines:
            components = line.strip().split(None, 7)
            datetime = components[0]
            frequency = components[1]
            rx = components[2] == 'Rx'
            mode = components[3]
            snr = int(components[4])
            delta_t = float(components[5])
            delta_f = int(components[6])
            message = components[7]

            if rx:
                pkt = pywsjtx.DecodePacket.Builder(delta_f=delta_f, delta_t=delta_t, snr=snr, mode="~",
                        message=message, low_confidence=0, off_air=0, new_decode=1)
                the_packet = pywsjtx.WSJTXPacketClassFactory.from_udp_packet([FORWARDING_PORTS[0]['ip_address'], FORWARDING_PORTS[0]['port']], pkt)
                print(the_packet)
                for s in servers:
                    s['instance'].send_packet(s['to_addr_port'], pkt)

if __name__ == "__main__":
        main()
